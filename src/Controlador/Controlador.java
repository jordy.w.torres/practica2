/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador;
import Modelo.*;
import Exception.PosicionException;
import java.util.ArrayList;
import java.util.List;
import org.github.jamm.MemoryMeter;
/**
 *
 * @author Jordy
 */
public class Controlador {
     MemoryMeter MedirMemoria = MemoryMeter.builder().build();
     ListaEnlazada lis = new ListaEnlazada();
     
    public void datoString(String dato){
       
        System.out.println("----Dato string Vacio---\n"+MedirMemoria.measureDeep(dato)+ "bits");
        System.out.println("---Dato String lleno---");
        String m = "Holaaasasasasssssss";
        dato = m;
        System.out.println(MedirMemoria.measureDeep(dato)+ "bits");
    }
    public void datoInteger(int entero){
         System.out.println("----Dato Entero Vacio---\n"+MedirMemoria.measureDeep(entero)+ "bits");
         System.out.println("----Dato Entero lleno---");
         int m = 2323;
         entero = m;
         System.out.println(MedirMemoria.measureDeep(entero)+ "bits");
    }
    public void datoDouble(double dou){
        System.out.println("----Dato Double Vacio---\n"+MedirMemoria.measureDeep(dou)+ "bits");
         System.out.println("----Dato Double lleno---");
         double m =234234234;
         dou = m;
         System.out.println(MedirMemoria.measureDeep(dou)+ "bits");
    }
    public void datoShort(Short sh){
        System.out.println("----Dato Short Vacio---\n"+MedirMemoria.measureDeep(sh)+ "bits");
         System.out.println("----Dato Short lleno---");
         Short m = 123;
         sh = m;
         System.out.println(MedirMemoria.measureDeep(sh)+ "bits");
    }
    public void datoLong(long ln){
        System.out.println("----Dato Long Vacio---\n"+MedirMemoria.measureDeep(ln)+ "bits");
         System.out.println("----Dato Long lleno---");
         long m = 1238978;
         ln = m;
         System.out.println(MedirMemoria.measureDeep(m)+ "bits");
    }
    public void datoFloat(float fl){
        System.out.println("----Dato Float Vacio---\n"+MedirMemoria.measureDeep(fl)+ "bits");
         System.out.println("----Dato Float lleno---");
         float m = 123;
         fl = m;
         System.out.println(MedirMemoria.measureDeep(fl)+ "bits");
    }
    public void datoboolean(boolean sh){
        System.out.println("----Dato Boolean Vacio---\n"+MedirMemoria.measureDeep(sh)+ "bits");
         System.out.println("----Dato Boolean lleno---");
         boolean m = true;
         sh = m;
         System.out.println(MedirMemoria.measureDeep(sh)+"bits");
    }
    public void datoByte(byte by){
        System.out.println("----Dato BYte Vacio---\n"+MedirMemoria.measureDeep(by)+ "bits");
         System.out.println("----Dato byte lleno---");
         byte m = 123;
         by = m;
         System.out.println(MedirMemoria.measureDeep(by)+ "bits");
    }

    public void datoChar(char ch){
        System.out.println("----Dato Char Vacio---\n"+MedirMemoria.measureDeep(ch)+ "bits");
         System.out.println("----Dato Char lleno---");
         char m = 't';
         ch = m;
         System.out.println(MedirMemoria.measureDeep(ch)+ "bits");
    }
    
    public void arrayList(ArrayList a){
        System.out.println("--Array list Vacio--\n"+MedirMemoria.measureDeep(a)+ "bits");
        System.out.println("--Array list lleno--");
        a.add("hola");
        a.add("carta");
        a.add("Telefono");
        a.add("mochila");
        a.add("mesa");
        a.add("casas");
        a.add("ciudad");
        a.add("tierra");
        a.add("planta");
        a.add("bola");
        a.add("lentes");
        a.add("casino");
        a.add("dinero");
        a.add("camisa");
        a.add("camiseta");
        a.add("chompa");
        a.add("toalla");
        a.add("pantalla");
        a.add("Computadora");
        a.add("taclado");
        a.add("audifonos");
        a.add("cargador");
        a.add("camara");
        a.add("tecla");
        a.add("cuaderno");
        a.add("carta");
        System.out.println(MedirMemoria.measureDeep(a)+ "bits");
    }
    public void arreglo1(String[] a){
        System.out.println("----Arreglo String Vacio---\n"+MedirMemoria.measureDeep(a)+ "bits");
         System.out.println("----Arreglo String lleno---");
         String[] p = {"as","as","as","as","as"};
         a = p;
         System.out.println(MedirMemoria.measureDeep(a)+ "bits");
    }
    public void arreglo2(int[] a){
        System.out.println("----Arreglo int Vacio---\n"+MedirMemoria.measureDeep(a)+ "bits");
         System.out.println("----Arreglo int lleno---");
         int[] p = {3,23,34,342,456};
         a = p;
         System.out.println(MedirMemoria.measureDeep(a)+ "bits");
    }
    public void arreglo3(double[] a){
        System.out.println("----Arreglo double Vacio---\n"+MedirMemoria.measureDeep(a)+ "bits");
         System.out.println("----Arreglo double lleno---");
         double[] p = {453223.3,452.5,452.56,676.23,545.2};
         a = p;
         System.out.println(MedirMemoria.measureDeep(a)+ "bits");
    }
    public void arreglo4(float[] a){
        System.out.println("---- Arreglo Flotantes vacios---\n"+MedirMemoria.measureDeep(a)+ "bits");
         System.out.println("----Arreglo flotantes lleno---");
         float[] p = {34.56f,634.23f,43.5f,35.43f,78.34f};
         a = p;
         System.out.println(MedirMemoria.measureDeep(a)+ "bits");
    }
    public void arreglo5(char[] a){
        System.out.println("---- arreglo de char  Vacio---\n"+MedirMemoria.measureDeep(a)+ "bits");
         System.out.println("----arreglo de char lleno---");
         char[] p = {'p','r','m','k','t'};
         a = p;
         System.out.println(MedirMemoria.measureDeep(a)+ "bits");
    }
    public void listaenlazada(ListaEnlazadaServices a)throws PosicionException{
        System.out.println("---- ListaEnlazada  Vacia---\n"+MedirMemoria.measureDeep(a)+ "bits");
         System.out.println("----ListaEnlazada lleno---");
         a.insertarAlInicio(5);
         a.insertarAlInicio(4);
         a.insertarAlInicio("computadora");
         a.insertarAlInicio("ciuda");
         a.insertarAlInicio("casa");
         a.insertarAlInicio("cosa");
         a.insertarAlInicio("tasa");
         a.insertarAlInicio("mesa");
         a.insertarAlInicio("carrro");
         a.insertarAlInicio("carta");
         a.insertarAlInicio("cuaderno");
         a.insertarAlInicio("esfero");
         a.insertarAlInicio("telefono");
         a.insertarAlInicio("copiadora");
         a.insertarAlInicio("pantalla");
         a.insertarAlInicio(34);
         a.insertarAlInicio(45);
         a.insertarAlInicio(676);
         a.insertarAlInicio(567);
         a.insertarAlInicio(234);
         a.insertarAlInicio(5464);
         a.insertarAlInicio(4563);
         a.insertarAlInicio(67);
         a.insertarAlInicio(54);
         a.insertarAlInicio(123);
         a.insertarAlInicio(2003);
         System.out.println(MedirMemoria.measureDeep(a)+ "bits");
         
    }
    
    
    
}
