/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author Jordy
 */
public class Primitivos {
    private String datoString="";
    private Integer datoInteger=0;
    private double datoDouble=0.0;
    private short datoShort;
    private long datolong;
    private float datoFloat;
    private boolean datoBoolean;
    private byte datoByte;
    private char datochar;
    
    private String datosStrings[] = new String[5];
    private int datosEnteros[] = new int[5];
    private double datosdouble[] = new double[5];
    private float datosFloat[] = new float[5];
    private char datosChar[] = new char[5];
    
    
    public String getDatoString() {
        return datoString;
    }

    /**
     * @param datoString the datoString to set
     */
    public void setDatoString(String datoString) {
        this.datoString = datoString;
    }

    /**
     * @return the datoInteger
     */
    public int getDatoInteger() {
        return datoInteger;
    }

    /**
     * @param datoInteger the datoInteger to set
     */
    public void setDatoInteger(int datoInteger) {
        this.datoInteger = datoInteger;
    }

    /**
     * @return the datoDouble
     */
    public double getDatoDouble() {
        return datoDouble;
    }

    /**
     * @param datoDouble the datoDouble to set
     */
    public void setDatoDouble(double datoDouble) {
        this.datoDouble = datoDouble;
    }

    /**
     * @return the datoShort
     */
    public short getDatoShort() {
        return datoShort;
    }

    /**
     * @param datoShort the datoShort to set
     */
    public void setDatoShort(short datoShort) {
        this.datoShort = datoShort;
    }

    /**
     * @return the datolong
     */
    public long getDatolong() {
        return datolong;
    }

    /**
     * @param datolong the datolong to set
     */
    public void setDatolong(long datolong) {
        this.datolong = datolong;
    }

    /**
     * @return the datoFloat
     */
    public float getDatoFloat() {
        return datoFloat;
    }

    /**
     * @param datoFloat the datoFloat to set
     */
    public void setDatoFloat(float datoFloat) {
        this.datoFloat = datoFloat;
    }

    /**
     * @return the datoBoolean
     */
    public boolean isDatoBoolean() {
        return datoBoolean;
    }

    /**
     * @param datoBoolean the datoBoolean to set
     */
    public void setDatoBoolean(boolean datoBoolean) {
        this.datoBoolean = datoBoolean;
    }

    /**
     * @return the datoByte
     */
    public byte getDatoByte() {
        return datoByte;
    }

    /**
     * @param datoByte the datoByte to set
     */
    public void setDatoByte(byte datoByte) {
        this.datoByte = datoByte;
    }

    /**
     * @return the datochar
     */
    public char getDatochar() {
        return datochar;
    }

    /**
     * @param datochar the datochar to set
     */
    public void setDatochar(char datochar) {
        this.datochar = datochar;
    }

    /**
     * @return the datosStrings
     */
    public String[] getDatosStrings() {
        return datosStrings;
    }

    /**
     * @param datosStrings the datosStrings to set
     */
    public void setDatosStrings(String[] datosStrings) {
        this.datosStrings = datosStrings;
    }

    /**
     * @return the datosEnteros
     */
    public int[] getDatosEnteros() {
        return datosEnteros;
    }

    /**
     * @param datosEnteros the datosEnteros to set
     */
    public void setDatosEnteros(int[] datosEnteros) {
        this.datosEnteros = datosEnteros;
    }

    /**
     * @return the datosdouble
     */
    public double[] getDatosdouble() {
        return datosdouble;
    }

    /**
     * @param datosdouble the datosdouble to set
     */
    public void setDatosdouble(double[] datosdouble) {
        this.datosdouble = datosdouble;
    }

    /**
     * @return the datosFloat
     */
    public float[] getDatosFloat() {
        return datosFloat;
    }

    /**
     * @param datosFloat the datosFloat to set
     */
    public void setDatosFloat(float[] datosFloat) {
        this.datosFloat = datosFloat;
    }

    /**
     * @return the datosChar
     */
    public char[] getDatosChar() {
        return datosChar;
    }

    /**
     * @param datosChar the datosChar to set
     */
    public void setDatosChar(char[] datosChar) {
        this.datosChar = datosChar;
    }
    
}

