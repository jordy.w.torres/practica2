/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Vista;

import Controlador.Controlador;
import Modelo.*;
import Exception.PosicionException;
import java.util.ArrayList;
import java.util.List;
import org.github.jamm.MemoryMeter;

/**
 *
 * @author Jordy
 */
public class main extends Primitivos {
    
    public static void main(String[] args) throws PosicionException{
     Controlador contro = new Controlador();
     Primitivos pri = new Primitivos();
     ArrayList<String> lista = new ArrayList<>();
     ListaEnlazadaServices lis = new ListaEnlazadaServices<>();
    
     
     String dato= pri.getDatoString();;
     contro.datoString(dato);
     Integer entero = pri.getDatoInteger();
     contro.datoInteger(entero);
     Double dou = pri.getDatoDouble();
     contro.datoDouble(dou);
     short sho = pri.getDatoShort();
     contro.datoShort(sho);
     long ln = pri.getDatolong();
     contro.datoLong(ln);
     float fl = pri.getDatoFloat();
     contro.datoFloat(fl);
     boolean bo = pri.isDatoBoolean();
     contro.datoboolean(bo);
     byte by = pri.getDatoByte();
     contro.datoByte(by);
     char ch = pri.getDatochar();
     contro.datoChar(ch);
//Arreglo 1 de Strings
     String[] arreglo1=pri.getDatosStrings();
     contro.arreglo1(arreglo1);
//Arreglo 2 de enteros
     int[] arreglo2 = pri.getDatosEnteros();
     contro.arreglo2(arreglo2);
// Arreglo 3 de double 
     double[] arreglo3 = pri.getDatosdouble();
     contro.arreglo3(arreglo3);
//Arreglo 4 de float 
     float[] arreglo4 = pri.getDatosFloat();
     contro.arreglo4(arreglo4);
//Arreglo 5 de char
     char[] arreglo5 = pri.getDatosChar();
     contro.arreglo5(arreglo5);
     
     
//ArrayList
     contro.arrayList(lista);
//listaEnlazada
      contro.listaenlazada(lis);
      
     
    }
}
